// Create a quiz object with a title and two questions.
// A question has one or more answer, and one or more is valid.

var quiz = { 
  user: 'Raimonds',	
  questions: [
    {
      text: "How far is up?",
      responses: [
        {text: 'Up'}, 
        {text: 'As far as you can see.', correct: true},
        {text: '36`000`000`000 km'}, 
        {text: 'Sun'},  
      ]
    }, {
      text: "How did you get here?",
      responses: [
        {text: 'I was always here', correct: true}, 
        {text: 'By plane'}, 
        {text: 'On foot'}, 
        {text: 'I hitchhiked'},  
      ]
    }, {
      text: "Are you aware of your clothes?",
      responses: [
        {text: 'Yes, I am now.', correct: true}, 
        {text: 'Wait what?'}, 
        {text: 'I am wearing a new pair of jeans, indeed'}, 
        {text: 'Go home, you are drunk'},  
      ]
    }, {
      text: "Is the answer to this question no?",
      responses: [
        {text: 'What?'}, 
        {text: 'Ok I am starting to get it'}, 
        {text: 'Yes, No, Yes, No, YesNoyesnoyesnoyesnooo....', correct: true}, 
        {text: 'I know, it was the previous answer'},  
      ]
    }, {
      text: "If you try to fail, and succeed, have you lost or won?",
      responses: [ 
        {text: 'Not this again'}, 
        {text: 'I am starting to get the duality of my own being.', correct: true}, 
        {text: 'Ok, I get it, I lost at succeeding'}, 
        {text: 'R#@%IWIF#@R)FQ'},  
      ]
    }, {
      text: "Is it crazy how saying sentences backwards creates backwards sentences saying how crazy it is?",
      responses: [
        {text: 'Wait..'}, 
        {text: 'Even the meaning is backwards'}, 
        {text: 'One more please'},  
        {text: 'Ok, this one blew my mind', correct: true}, 
      ]
    }, {
      text: "How many times you had blinked a minute ago?",
      responses: [
        {text: '23'}, 
        {text: 'Square root of Pi times! Whos the smart455 now?'}, 
        {text: 'Now I am are aware of my blinking, thank you!', correct: true}, 
        {text: 'I didnt even read this question'},  
      ]
    }, {
      text: "What thought you have never had?",
      responses: [
        {text: 'This thought'}, 
        {text: 'I can not answer this.', correct: true}, 
        {text: 'a sandwich'}, 
        {text: 'The plot of Harry Potter'},  
      ]
    }, {
      text: "If you clean your Vacuum cleaner, does that make you the vacuum cleaner",
      responses: [
      	{text: 'It certainly makes the vacuum cleaner!', correct: true}, 
        {text: 'My vacuum is always clean'}, 
        {text: 'How do you clean a vacuum cleaner'}, 
        {text: 'Never'},  
      ]
    }, {
      text: "Is water wet?",
      responses: [
        {text: 'Of course it is'}, 
        {text: 'What?'}, 
        {text: 'Where`s the catch in this one?'},
        {text: 'Water itself is not inherently wet.', correct: true},   
      ]
    }, {
      text: "What is the color of mirror?",
      responses: [
        {text: 'Mirror-colored'}, 
        {text: 'Silver'}, 
        {text: 'Of course it is chrome'},
        {text: 'Most mirrorss are green', correct: true},   
      ]
    }             
  ]
};
      
var app = new Vue({
  el: '#app',
  data: {
    quiz: quiz,
    // Store current question index
    questionIndex: 0,
    // An array initialized with "false" values for each question
    // It means: "did the user answered correctly to the question n?" "no".
    userResponses: Array(quiz.questions.length).fill(false),
    isActive: false,
    myWidth: 0,
    prog: 0,
  },
  // The view will trigger these methods on click
  methods: {
    // Go to next question
    roundProg: function(){
    	this.prog = Math.round(this.myWidth);
    },
    select: function(){
    	isActive = true;
    	console.log(this);
    },
    next: function() {
      this.questionIndex++;
      this.myWidth=this.questionIndex/quiz.questions.length*100;
    },
    // Go to previous question
    prev: function() {
      this.questionIndex--;
      this.myWidth=this.questionIndex/quiz.questions.length*100;
    },
    // Return "true" count in userResponses
    score: function() {
      return this.userResponses.filter(function(val) { return val }).length;
    }
  }
});